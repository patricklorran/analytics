//
//  AnalyticsManager.swift
//  Analytics
//
//  Created by Patrick Lorran on 24/09/20.
//

import Firebase
import Foundation

final class AnalyticsManager {
    
    func logScreenView(_ name: String) {
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [
            AnalyticsParameterScreenName: name
        ])
    }
    
    func logEvent(_ name: String, parameters: [String: Any]) {
        Analytics.logEvent(name, parameters: parameters)
    }
    
    func setUserProperties(_ properties: [String: String]) {
        properties.forEach({ Analytics.setUserProperty($1, forName: $0) })
    }
}
