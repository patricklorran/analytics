//
//  Foundation+LocalizableString.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import Foundation

func LocalizedString(_ key: String) -> String {
    NSLocalizedString(key, comment: .empty)
}
