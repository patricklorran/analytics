//
//  FormViewModel.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import Hasher
import Foundation

final class FormViewModel {
    
    private let analyticsManager: AnalyticsManager = .init()
    
    let navigationBarTitle: String = LocalizedString("FormView.NavigationBar.Title")
    let formFooterText: String = LocalizedString("FormView.Form.Footer")
    
    let emailTitle: String = LocalizedString("FormView.Form.Email.Title")
    let ageTitle: String = LocalizedString("FormView.Form.Age.Title")
    
    let sumbitButtonTitle: String = LocalizedString("FormView.SubmitButton.Title")
    
    func logScreenView() {
        analyticsManager.logScreenView("form")
    }
    
    func sumbit(email: String, age: String) {
        guard let data = email.data(using: .utf8),
              let hash = Hasher.hash(data, algorithm: .md5) else {
            return
        }
        
        analyticsManager.logEvent("interaction", parameters: [
            "eventCategory": "form",
            "eventAction": "submit",
            "eventLabel": "form submitted",
            "screen_name": "form"
        ])
        
        analyticsManager.setUserProperties([
            "email": hash,
            "idade": age
        ])
    }
}
