//
//  String+Empty.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import Foundation

extension String {
    
    static let empty: String = ""
}
