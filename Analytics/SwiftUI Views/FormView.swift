//
//  FormView.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import SwiftUI

struct FormView: View {
    
    private let viewModel: FormViewModel = .init()
    
    @State
    private var email: String = .empty
    
    @State
    private var age: String = .empty
    
    var body: some View {
        VStack {
            Form {
                Section(footer: Text(viewModel.formFooterText)) {
                    TextField(viewModel.emailTitle, text: $email)
                        .keyboardType(.emailAddress)
                    TextField(viewModel.ageTitle, text: $age)
                        .keyboardType(.numberPad)
                }
                Section {
                    Button(viewModel.sumbitButtonTitle) {
                        viewModel.sumbit(email: email, age: age)
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                }
            }
        }
        .navigationBarTitle(viewModel.navigationBarTitle)
        .onAppear() {
            viewModel.logScreenView()
        }
    }
}

struct FormViewPreviews: PreviewProvider {
    
    static let viewModel: FormViewModel = .init()
    
    static var previews: some View {
        NavigationView {
            FormView()
        }
        .navigationBarTitle(viewModel.navigationBarTitle)
    }
}
