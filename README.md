# Firebase Analytics

A sample project that demonstrates Firebase Analytics and Google Tag Manager integration on iOS.

## Setup

This project uses Cocoapods for dependency management. Run `$ pod install` after cloning the repository to install the required frameworks.

Also, this projects makes use of [Hasher](https://gitlab.com/patricklorran/hasher), an open-source Swift Package for quick hash calculations built upon Apple's Commom Crypto.

## Usage

You will need to create and configure a project on Firebase and Google Tag Manager platforms, so you will be able to add your own **GoogleService-Info.plist** and **GTM-ABCDEFG.json** files.

* [Add Firebase to your iOS project](https://firebase.google.com/docs/ios/setup)
* [Tag Manager + Firebase: Getting Started](https://developers.google.com/tag-manager/ios/v5)

With everything configured correcly, just build and run the app. Once you start tapping and navigating, you will be able to see the hits on the dashboards.