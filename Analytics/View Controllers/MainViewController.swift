//
//  ViewController.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import UIKit

final class MainViewController: UIViewController {

    private var tableView: UITableView = .init(frame: .zero, style: .insetGrouped)
    
    private let viewModel: MainViewModel = .init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.navigationBarTitle
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        configureTableView()
        
        viewModel.load()
        viewModel.logScreenView()
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate   = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
    }
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.tableViewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        viewModel.tableViewFooter
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = .init()
        let item: Item = viewModel.items[indexPath.row]
        
        cell.textLabel?.text = item.title
        cell.accessoryType = item.redirect == .event ? .none : .disclosureIndicator
        
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = viewModel.items[indexPath.row]
        let redirect = viewModel.redirect(with: item)
        
        switch redirect {
            case .event(let name):
                viewModel.logEvent(name, label: item.title)
            case .navigation(let viewController):
                navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
