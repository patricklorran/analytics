//
//  MainViewModel.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import Foundation

import class UIKit.NSDataAsset
import class UIKit.UIViewController

import class SwiftUI.UIHostingController

final class MainViewModel {
    
    private let analyticsManager: AnalyticsManager = .init()
    
    let navigationBarTitle: String = LocalizedString("MainViewController.NavigationBar.Title")
    
    var tableViewHeader: String {
        LocalizedString("MainViewController.TableView.Header")
    }
    
    var tableViewFooter: String {
        let bundleShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? .empty
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? .empty
        
        return .init(format: LocalizedString("MainViewController.TableView.Footer"), bundleShortVersion, bundleVersion)
    }
    
    var items: [Item] = []
    
    private let decoder: JSONDecoder = .init()
    
    func load() {
        guard let asset = NSDataAsset(name: "Items"),
              let items = try? decoder.decode([Item].self, from: asset.data) else {
            return
        }
        
        self.items = items
    }
    
    func redirect(with item: Item) -> Redirect {
        switch item.redirect {
            case .event:
                return .event(label: item.label)
            case .form:
                return .navigation(viewController: UIHostingController(rootView: FormView()))
        }
    }
    
    func logScreenView() {
        analyticsManager.logScreenView("menus")
    }
    
    func logEvent(_ name: String, label: String) {
        analyticsManager.logEvent(name, parameters: [
            "eventCategory": "menu",
            "eventAction": "click",
            "eventLabel": label,
            "screen_name": "menus"
        ])
    }
}

extension MainViewModel {
    
    @frozen enum Redirect {
        case event(label: String)
        case navigation(viewController: UIViewController)
    }
}
