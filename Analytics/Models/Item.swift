//
//  Item.swift
//  Analytics
//
//  Created by Patrick Lorran on 18/09/20.
//

import Foundation

struct Item: Codable {
    
    let title: String
    let label: String
    let redirect: Redirect
}

extension Item {
    
    @frozen enum Redirect: String, Codable {
        case event
        case form
    }
}
